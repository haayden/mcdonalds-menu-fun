const fs = require('fs');
const jeItemNamesToMcItemNames = require('./jeItemNamesToMcItemNames.json');

function writeFileSync(file, data) {
  fs.writeFileSync(
    file,
    data,
    { flag: 'w+' }, // w+ creates file or overwrites contents
  );
  console.log(`File "${file}" written successfully.`);
}

// Just Eat and McDonalds item names vary greatly, this maps their names 1-to-1
function normaliseName(oldName) {
  let name = oldName.replaceAll('&', 'and')
    .replaceAll('®.', '')
    .replaceAll('®', '')
    .replaceAll('™', '')
    .replaceAll('�', '')
    .replaceAll('[VE]', '')
    .replaceAll('–', '-')
    .replaceAll('  ', ' ') // Double space to single
    .replaceAll('McCafé ', '')
    .replaceAll('McCaf ', '')
    .replaceAll(' - small', ' Small')
    .replaceAll(' - Small', ' Small')
    .replaceAll(' - large', ' Large')
    .replaceAll(' - Large', ' Large')
    .replaceAll(' - regular', '')
    .replaceAll(' - Regular', '')
    .replaceAll('Regular', '')
    .replaceAll('regular', '')
    .replaceAll('Medium', '')
    .replaceAll('McDonald\'s ', '') // Mainly to remove Fries prefix
    .replaceAll('Wrap of the day:', '')
    .replaceAll('Wrap of the day', '')
    .replaceAll('Wrap of the Day', '')
    .replaceAll('(Crispy)', '- Crispy')
    .replaceAll('(Grilled)', '- Grilled')
    .replaceAll('andBacon', 'and Bacon')
    .trim();

  if (name.includes('Small ')) name = `${name.replaceAll('Small ', '')} Small`;
  if (name.includes('Large ')) name = `${name.replaceAll('Large ', '')} Large`;
  if (name.includes('Mini ')) name = `${name.replaceAll('Mini ', '')} Mini`;

  const mappedName = jeItemNamesToMcItemNames[name];
  if (mappedName) name = mappedName;

  return name;
}

function addItemPriceToRestaurantData(restaurantData, itemName, itemPrice) {
  const niceName = normaliseName(itemName);
  const priceString = itemPrice.toString();
  // eslint-disable-next-line no-param-reassign
  restaurantData.itemPrices[niceName] = priceString;
}

function stringIsJson(string) {
  return string.startsWith('{');
}

module.exports = {
  writeFileSync,
  normaliseName,
  addItemPriceToRestaurantData,
  stringIsJson,
};
