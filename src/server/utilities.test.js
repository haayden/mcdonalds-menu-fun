const fs = require('fs');
const {
  writeFileSync,
  normaliseName,
  addItemPriceToRestaurantData,
  stringIsJson,
} = require('./utilities');

test('function writeFileSync writes file data as expected', () => {
  const filePath = 'tmp/test.txt';
  const fileData = 'test file content';
  writeFileSync(filePath, fileData);
  expect(
    fs.readFileSync(filePath, 'utf8'),
  ).toEqual(fileData);
});

test('function normaliseName transforms string as expected', () => {
  expect(
    normaliseName('Regular Fries'),
  ).toBe('Fries');
});

test('function addItemPriceToRestaurantData mutates data as expected', () => {
  const restaurantData = {
    itemPrices: {
      Cheeseburger: '0.99',
    },
  };
  const restaurantDataAfter = {
    itemPrices: {
      Cheeseburger: '0.99',
      Fries: '1.19',
    },
  };

  addItemPriceToRestaurantData(restaurantData, 'Fries', '1.19');
  expect(restaurantData).toEqual(restaurantDataAfter);
});

test('function stringIsJson tests string as expected', () => {
  expect(
    stringIsJson('{"foo":"bar"}'),
  ).toBe(true);
  expect(
    stringIsJson('<HTML></HTML>'),
  ).toBe(false);
});
