const {
  normaliseName,
  writeFileSync,
} = require('./utilities');
/* eslint-disable import/no-unresolved */
const nutritionData = require('../../tmp/nutritionData.json');
const jeRestaurantsData = require('../../tmp/jeRestaurantsData.json');
/* eslint-disable import/no-unresolved */

const nutritionPriceTableData = {};
const priceDifferenceTableData = {};

function addToPriceDifferences(itemName, itemPrice, restaurantName) {
  const appendRName = `, ${restaurantName}`;

  // When item already exists
  if (Object.prototype.hasOwnProperty.call(priceDifferenceTableData, itemName)) {
    const { lowestPrice, highestPrice } = priceDifferenceTableData[itemName];

    // If equivalent lowest, add restaurant name to list
    if (lowestPrice === itemPrice) {
      priceDifferenceTableData[itemName].lowestRestaurants += appendRName;
    }
    // If new lowest, set new lowest price and single restaurant
    if (lowestPrice > itemPrice) {
      priceDifferenceTableData[itemName].lowestPrice = itemPrice;
      priceDifferenceTableData[itemName].lowestRestaurants = restaurantName;
    }
    // If equivalent highest, add restaurant name to list
    if (highestPrice === itemPrice) {
      priceDifferenceTableData[itemName].highestRestaurants += appendRName;
    }
    // If new highest, set new highest price and single restaurant
    if (highestPrice < itemPrice) {
      priceDifferenceTableData[itemName].highestPrice = itemPrice;
      priceDifferenceTableData[itemName].highestRestaurants = restaurantName;
    }

  // When item doesn't exist, create it
  } else {
    priceDifferenceTableData[itemName] = {
      lowestPrice: itemPrice,
      lowestRestaurants: restaurantName,
      highestPrice: itemPrice,
      highestRestaurants: restaurantName,
    };
  }
}

// Initialise place to collect a count of menu items availability
// so this can be incremented as restaurants are looped through
const itemAvailability = {};
Object.keys(nutritionData).forEach((itemName) => {
  itemAvailability[itemName] = 0;
});

// Collect additional computed data
const restaurantComputedData = {};

// For each Just Eat restaurant JSON database
Object.keys(jeRestaurantsData).forEach((rSlug) => {
  const restaurantData = jeRestaurantsData[rSlug];
  const rName = restaurantData.name;

  // Set key for each restaurant, add name, address and empty items to start
  nutritionPriceTableData[rName] = {
    name: rName,
    address: restaurantData.address,
    latitude: restaurantData.latitude,
    longitude: restaurantData.longitude,
    items: [],
  };

  // Collect restaurant slug and item count/price of all items added together
  restaurantComputedData[rSlug] = {
    itemCount: Object.keys(restaurantData.itemPrices).length,
    allItemsPrice: 0,
  };

  // For each restaurant item
  Object.entries(restaurantData.itemPrices).forEach(([name, price]) => {
    // Add price to highest/lowest price differences
    addToPriceDifferences(name, price, rName);
    // Increment restaurant item availability
    itemAvailability[name] += 1;
    // Add price of item as we loop through
    restaurantComputedData[rSlug].allItemsPrice += parseFloat(price);

    // Try and match Just Eat item name to McDonalds nutritional item name
    const itemData = nutritionData[name] || nutritionData[normaliseName(name)];

    if (!itemData) {
      console.log(`nutritionData[name] for name="${name}" not found.`);
    } else {
      const {
        category, energy_kcal: kcal, energy_kJ: kj,
        fat, saturated_fat: saturatedFat,
        carbohydrate, sugars: sugar, fibre, protein, salt,
      } = itemData;

      const kcalPerGbp = (kcal / price).toFixed(0);
      const kjPerGbp = (kj / price).toFixed(0);
      const fatPerGbp = (fat / price).toFixed(1);
      const saturatedFatPerGbp = (saturatedFat / price).toFixed(1);
      const carbohydratePerGbp = (carbohydrate / price).toFixed(1);
      const sugarPerGbp = (sugar / price).toFixed(1);
      const fibrePerGbp = (fibre / price).toFixed(2);
      const proteinPerGbp = (protein / price).toFixed(1);
      const saltPerGbp = (salt / price).toFixed(2);

      // Push to restaurant items an item that includes:
      // category, name, price, nutrient and nutrientPerGBP for all permutations
      nutritionPriceTableData[rName].items.push({
        category,
        name,
        price: parseFloat(price).toFixed(2),
        kcal,
        kcalPerGbp,
        kj,
        kjPerGbp,
        fat,
        fatPerGbp,
        saturatedFat,
        saturatedFatPerGbp,
        carbohydrate,
        carbohydratePerGbp,
        sugar,
        sugarPerGbp,
        fibre,
        fibrePerGbp,
        protein,
        proteinPerGbp,
        salt,
        saltPerGbp,
      });
    }
  });
});

// Now all data looped over, add final differences keys
Object.keys(priceDifferenceTableData).forEach((itemName) => {
  const { lowestPrice, highestPrice } = priceDifferenceTableData[itemName];

  // Add difference between prices
  priceDifferenceTableData[itemName].difference = (highestPrice - lowestPrice).toFixed(2);

  // Ensure price displays to 2 decimal places
  priceDifferenceTableData[itemName].highestPrice = parseFloat(highestPrice).toFixed(2);
  priceDifferenceTableData[itemName].lowestPrice = parseFloat(lowestPrice).toFixed(2);
});

const restaurantCount = Object.keys(nutritionPriceTableData).length;

// Collect all menu item nutrition in a single place, also including
// collected menu item restaurant availability
const allNutritionTableData = [];
Object.keys(nutritionData).forEach((itemName) => {
  const {
    energy_kcal: kcal, energy_kJ: kj,
    fat, saturated_fat: saturatedFat,
    carbohydrate, sugars: sugar, fibre, protein, salt,
  } = nutritionData[itemName];

  allNutritionTableData.push({
    itemName,
    kcal,
    kj,
    fat,
    saturatedFat,
    carbohydrate,
    sugar,
    fibre,
    protein,
    salt,
    availability: `${itemAvailability[itemName]} / ${restaurantCount}`,
  });
});

const menuItemCount = Object.keys(allNutritionTableData).length;

// Create data array for general restaurant table from collected computed data
const restaurantTableData = [];
Object.keys(restaurantComputedData).forEach((rSlug) => {
  const { name, ratingsAverage, ratingsCount } = jeRestaurantsData[rSlug];
  const { itemCount, allItemsPrice } = restaurantComputedData[rSlug];
  restaurantTableData.push({
    name,
    url: `https://www.just-eat.co.uk/restaurants-${rSlug}/menu`,
    itemAvailability: `${itemCount} / ${menuItemCount}`,
    itemAveragePrice: (allItemsPrice / itemCount).toFixed(2),
    ratingsAverage,
    ratingsCount,
  });
});

// Store prepared client data in ./tmp/ JSON files
writeFileSync(
  'tmp/clientTemplateData.json',
  JSON.stringify({
    restaurantNameOptions: Object.keys(nutritionPriceTableData),
    menuItemCount,
    restaurantCount,
    priceDifferenceTableData,
    restaurantTableData,
    allNutritionTableData,
  }, null, 2),
);
writeFileSync(
  'tmp/clientJsData.json',
  JSON.stringify({ nutritionPriceTableData }, null, 2),
);
