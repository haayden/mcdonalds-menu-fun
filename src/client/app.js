import Tablesort from 'tablesort';
import haversine from 'haversine'; // Gets distance between 2 lat-long points
import './styles/main.css';
import './styles/tablesort.css';
import './styles/tabs.css';

// eslint-disable-next-line import/no-unresolved
const { nutritionPriceTableData } = require('../../tmp/clientJsData.json');

// Workaround that enables data-sort-method="number" usage
window.Tablesort = Tablesort;
require('tablesort/src/sorts/tablesort.number');

// Lookup unchanging elements by ID once here and reference later
let restaurantChoiceEl = document.getElementById('restaurant-choice');
const findClosestEl = document.getElementById('find-closest');
const restaurantAddressEl = document.getElementById('restaurant-address');
const nutritionChoiceEl = document.getElementById('nutrition-choice');
const allButtonEls = document.querySelectorAll('button');

// Initialise table sorting once before they are shown
document.querySelectorAll('table').forEach((tableEl) => {
  let options = { descending: true };
  if (tableEl.id === 'all-nutrition-table') { options = { ascending: true }; }
  Tablesort(tableEl, options);
});

const toArray = (value) => {
  if (value instanceof window.NodeList) return value;
  return [value];
};
const show = (els) => toArray(els).forEach((el) => { el.style.display = 'block'; });
const hide = (els) => toArray(els).forEach((el) => { el.style.display = 'none'; });
const hideAllTables = () => hide(document.querySelectorAll('table'));
const disable = (els) => toArray(els).forEach((el) => el.setAttribute('disabled', true));
const enable = (els) => toArray(els).forEach((el) => el.removeAttribute('disabled'));
const enableAllButtons = () => enable(allButtonEls);

let lastRestaurantChoice;

function buildAndShowNutritionPriceTable() {
  // Remember/set/use last restaurant choice in case selection is cleared
  // and nutrition choice triggers a rebuild with an empty nutrition choice value
  let restaurantName = restaurantChoiceEl.value;
  if (restaurantName === '') restaurantName = lastRestaurantChoice;
  lastRestaurantChoice = restaurantName;

  const restaurantData = nutritionPriceTableData[restaurantName];
  const nutritionValue = nutritionChoiceEl.value;
  const nutritionSelectedIndex = nutritionChoiceEl.selectedIndex;
  let nutritionName = nutritionChoiceEl.options[nutritionSelectedIndex].text;

  restaurantAddressEl.innerText = restaurantData.address; // Show address

  // Shorten particularly long names
  if (nutritionName === 'Saturated Fat') nutritionName = 'Sat Fat';
  if (nutritionName === 'Carbohydrate') nutritionName = 'Carb';

  const headerTrEl = document.createElement('tr');
  [
    'Category', 'Item Name', 'Price', nutritionName, `${nutritionName} per £`,
  ].forEach((thText, index) => {
    const thEl = document.createElement('th');
    thEl.innerHTML = thText;
    if (index > 2) thEl.setAttribute('data-sort-method', 'number');
    if (index === 4) thEl.setAttribute('data-sort-default', 'true');
    headerTrEl.appendChild(thEl);
  });

  const theadEl = document.createElement('thead');
  theadEl.appendChild(headerTrEl);
  const tbodyEl = document.createElement('tbody');

  // For specific restaurant data and selected nutrition,
  // for each menu item create and add td elements to tr
  restaurantData.items.forEach((menuItem) => {
    const dataTrEl = document.createElement('tr');
    [
      'category', 'name', 'price', nutritionValue, `${nutritionValue}PerGbp`,
    ].forEach((dataKey, index) => {
      const tdEl = document.createElement('td');
      tdEl.innerHTML = menuItem[dataKey];
      if (index === 2) tdEl.classList.add('prepend-gbp');
      if (index > 2) {
        if (dataKey.includes('kj')) tdEl.classList.add('append-kj');
        else if (dataKey.includes('kcal')) tdEl.classList.add('append-kcal');
        else tdEl.classList.add('append-grams');
      }
      dataTrEl.appendChild(tdEl);
    });
    tbodyEl.appendChild(dataTrEl);
  });

  const newTableEl = document.createElement('table');
  newTableEl.id = 'nutrition-price-table';
  newTableEl.appendChild(theadEl);
  newTableEl.appendChild(tbodyEl);
  const oldTableEl = document.getElementById('nutrition-price-table');
  oldTableEl.parentNode.replaceChild(newTableEl, oldTableEl);
  hideAllTables();
  Tablesort(newTableEl, { descending: true });
  show(newTableEl);

  enableAllButtons();
  enable(nutritionChoiceEl);
  hide(findClosestEl);
}

function showTable(clickedButtonEl, tableId) {
  hideAllTables();
  const tableEl = document.getElementById(tableId);
  show(tableEl);

  enableAllButtons();
  disable(clickedButtonEl);

  restaurantChoiceEl.value = null;
  restaurantAddressEl.innerText = '';
  disable(nutritionChoiceEl);
  show(findClosestEl);
}

// Get user latitude/longitude and auto-select closest restaurant in distance
function findClosest() {
  if (!navigator.geolocation) {
    restaurantAddressEl.textContent = 'Geolocation is not supported by your browser';
  } else {
    navigator.geolocation.getCurrentPosition(
      // On success
      (position) => {
        const userLatitude = position.coords.latitude;
        const userLongitude = position.coords.longitude;

        let closestRestaurantName;
        let closestRestaurantDistance;

        // Loop over every restaurant
        Object.keys(nutritionPriceTableData).forEach((restaurantName, index, array) => {
          const {
            latitude: rLatitude, longitude: rLongitude,
          } = nutritionPriceTableData[restaurantName];

          const distance = haversine(
            { latitude: userLatitude, longitude: userLongitude },
            { latitude: rLatitude, longitude: rLongitude },
          );

          // When first time, or distance is less than closest, set that as new closest
          if (index === 0 || distance < closestRestaurantDistance) {
            closestRestaurantName = restaurantName;
            closestRestaurantDistance = distance;
          }

          // For final array iteration
          if (index === array.length - 1) {
            // Set closest restaurant as input choice
            restaurantChoiceEl.value = closestRestaurantName;
            // Show table with closest restaurant choice
            buildAndShowNutritionPriceTable();
            // Hide display of find-closest
            hide(findClosestEl);
          }
        });
      },
      // On error
      () => {
        restaurantAddressEl.textContent = 'Geolocation is not supported by your browser';
      },
    );
  }
}

function changeTab(tabLinkEl, tabContentId) {
  document.getElementsByClassName('active-tab')[0].classList.remove('active-tab');
  tabLinkEl.classList.add('active-tab');

  const allTabEls = document.querySelectorAll('.tabs-content .tab-content');
  hide(allTabEls);

  const tabEl = document.getElementById(tabContentId);
  show(tabEl);
}

// Datalist Inputs have horrible UX on Firefox, so swap in Select instead
if (navigator.userAgent.includes('Firefox')) {
  const inputEl = restaurantChoiceEl;
  const selectEl = document.createElement('select');
  selectEl.setAttribute('id', 'restaurant-choice');
  selectEl.addEventListener('change', buildAndShowNutritionPriceTable);

  // Create special option to act as placeholder text
  const firstOptionEl = document.createElement('option');
  firstOptionEl.text = 'Click to select';
  firstOptionEl.setAttribute('selected', true);
  disable(firstOptionEl);
  firstOptionEl.setAttribute('hidden', true);
  selectEl.add(firstOptionEl);

  // Create select options from datalist
  const datalistEl = document.getElementById('restaurant-names');
  for (const { value } of datalistEl.children) {
    const optionEl = document.createElement('option');
    optionEl.text = value;
    selectEl.add(optionEl);
  }

  // Replace input element with select element
  inputEl.parentNode.replaceChild(selectEl, inputEl);
  restaurantChoiceEl = selectEl; // Keep element reference up to date
}

// Initialise all click/change event listeners
document.getElementById('nutrition-per-gbp-view-tab').addEventListener('click', (e) => {
  changeTab(e.currentTarget, 'nutrition-per-gbp-view-tab-content');
});
document.getElementById('other-menu-views-tab').addEventListener('click', (e) => {
  changeTab(e.currentTarget, 'other-menu-views-tab-content');
});
restaurantChoiceEl.addEventListener('change', buildAndShowNutritionPriceTable);
// Specific listeners only for datalist input Restaurant Choice which
// ensures full datalist of options shows again if user wants to re-choose
const datalistInputEl = document.querySelector('input#restaurant-choice');
if (datalistInputEl) {
  datalistInputEl.addEventListener('focus', (e) => { e.currentTarget.value = ''; });
  datalistInputEl.addEventListener('change', (e) => { e.currentTarget.blur(); });
}
findClosestEl.addEventListener('click', findClosest);
nutritionChoiceEl.addEventListener('change', buildAndShowNutritionPriceTable);
allButtonEls[0].addEventListener('click', (e) => {
  showTable(e.currentTarget, 'price-difference-table');
});
allButtonEls[1].addEventListener('click', (e) => {
  showTable(e.currentTarget, 'restaurant-table');
});
allButtonEls[2].addEventListener('click', (e) => {
  showTable(e.currentTarget, 'all-nutrition-table');
});
// Expand long restaurant name lists in #price-difference-table
document.addEventListener(
  'click',
  (e) => {
    if (e.target.hasAttribute('data-full-value')) {
      const restaurantNames = e.target.getAttribute('data-full-value');
      const ulEl = document.createElement('ul');

      restaurantNames.split(', ').forEach((restaurantName) => {
        const liEl = document.createElement('li');
        liEl.innerHTML = restaurantName;
        ulEl.appendChild(liEl);
      });

      const tdEl = e.target.parentNode.parentNode;
      tdEl.replaceChild(ulEl, tdEl.childNodes[0]);
    }
  },
);
