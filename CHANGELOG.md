# Change log

## 2024-10-27 changes

- Modify `server/scrape.js` to:
  - Fix getting data from new Next.js version of Just Eat website
  - Abort many more redundant requests we don't need
  - Fix nutrition errors for empty categories and OPTIONS requests
- Modify `server/utilities.js` to remove `[VE]` from any Just Eat product name
- Update all packages to latest and fix deprecations
- Use Node.js 22 and Alpine 3.19 in Dockerfile and Gitlab CI

## 2024-01-17 changes

- Modify `server/scrape.js` to:
  - Improve resilience against navigation timeout errors that happen often in CI/CD when calling Puppeteer `page.waitForResponse()` function
- Add new item-name-maps in `server/jeItemNamesToMcItemNames.json`
- Update caniuse-lite package

## 2023-10-25 changes

- Modify `server/scrape.js` to
  - Ensure all Nutritional SPA JSON API calls do not halt script execution if non-JSON is returned
  - Remove single products that are added by Nutritional SPA What's New category

## 2023-10-22 changes

- Modify `server/scrape.js` to:
  - Improve resilience. If some JSON API calls returns status 200, but actually do not return valid JSON, the script will detect this and no longer error but instead skip the item
  - Use new `server/utilities.js` function `stringIsJson`
  - Not request tracking URLs containing `demdex`
  - Remove discontinued single products and add new Halloween/Autumn 2023 ones
- Update `package-lock.json` using `npm audit fix`

## 2023-10-09 changes

- Update npm package puppeteer to new major
- Update postcss package to fix CVE
- Fix Just Eat restaurant scraping by modifying `server/scrape.js` to remove Headless from announced user agent
- Improve name normalising in `server/utilities.js`
- Add new item-name-maps in `server/jeItemNamesToMcItemNames.json`

## 2023-08-02 changes

- Modify `server/scrape.js` to:
  - Improve resilience against navigation timeout errors that happen often in CI/CD
  - Remove discontinued single products

## 2023-07-19_2 changes

- Add columns Avg Rating and Reviews to Restaurant table
- Shorten Restaurant table column names to make room for new columns
- Update JE restaurants-to-scrape in `src/server/jeRestaurantSlugs.json` so 404 ones are removed, and some new branches are added. 105 total

## 2023-07-19 changes

- Fix transient scrape error when waiting for category itemList API call by looking for more specific URL. Error would occur if product itemList was found instead, which happened at least once a week in CI/CD
- Fix docker-compose.yml incorrect environment variable

## 2023-07-15 changes

- Amend webpack configuration to:
  - Add/fix JS minification
  - Add/fix creation of vendors chunk
  - Remove creation of runtime chunk

## 2023-07-14 changes

- Update Gitlab CI Docker base image to use Node.js 20 and Alpine 3.18

## 2023-07-13 changes

- Update all package.json dependencies to latest, addressing a few CVEs
- Update Dockerfile to use Node.js 20 and Alpine 3.18
- Use Chrome's headless=new mode when scraping and testing
- Modify `server:scrape` to:
  - No longer modify user agent
  - Add new single product scrapes and rework categorising code for this
  - Output extra console output to help debug transient scraping error in daily Continuous Deployment
- Improve name normalising in `server/utilities.js`
- Updated CSS to follow updated stylelint rules

## 2023-04-11 changes

- In `server/scrape.js` for single-product scraping:
  - Add logic so execution will gracefully continue if previously active products become discontinued
  - Remove several discontinued products and their category mappings
- Add new item-name-maps for Mighty McMuffin and Sprite Zero in `server/jeItemNamesToMcItemNames.json`

## 2023-03-09 changes

- DRY up `server/scrape.js` so function `addItemToNutritionData` does code in a single place that was previously in a few places
- Improve Small/Large/Mini naming normalisation between Just Eat and McDonald's Nutritional Calculator product names
- Improve wrap name normalisation so it's less hardcoded and more likely to automatically work if/when new wrap products are added
- Remove `Mcdonald's` from product names as it's redundant
- Remove possibility of price-difference-table lowestRestaurants/highestRestaurants column values showing `All` as this doesn't make sense for items that are not available everywhere
- Add extra single products scrapes for `Aero Mint McFlurry`

## 2023-03-08 changes

- Fix `server/scrape.js` as McDonald's Nutritional Calculator changed their API behaviour and this meant nutritional information for product sizes was no longer being scraped. Before when clicking in to a category, the API returned all item details for that category in a single request, but this changed to only returning the 'base' items (e.g. Regular Fries, but not Large Fries or Small Fries). To fix this, the scraper now must click in to each category item, and use the HTML select to pick each item size, to then capture an API request for each one.
- Add extra single products scrapes for: `Aero Chocolate McFlurry`, `Cheesy Garlic Bites`, `Cucumber Sticks`, `Melon Fruit Bag`, `The Spicy Sriracha Chicken One` and `Raspbery Ripple Iced Cooler`.
- Remove single product scrapes for: `Pineapple Stick` and `Galaxy Chocolate McFlurry`
- Resolve many new product-name-mapping issues in `src/server/jeItemNamesToMcItemNames.json`
- Use `npm audit fix` to update vulnerable dependencies

## 2022-08-03 changes

- Update restaurant-to-scrape in `src/server/jeRestaurantSlugs.json` so 404 ones are removed, and some new branches are added
- Resolve many new product-name-mapping issues in `src/server/jeItemNamesToMcItemNames.json`
- Modify `server:scrape` function `scrapeMcDonaldsNutritionalCalculator` to fix it to work with the new HTML page structure and API URL path & response data structure

## 2022-08-02 changes

- Remove discontinued Single Product scrape of mozzarella dippers which was causing `server:scrape` to timeout

## 2022-06-25 changes

- Rename `README.MD` to `README.md`
- Fix `CHANGELOG.md` incorrect year dates

## 2022-04-01 changes

- Modify `server:scrape` to:
  - No longer access access JSON API to get McDonald's Nutritional Calculator hidden category data, as it's no longer hidden
  - Assign correct categories for McDonald's single product page scrapes
  - Adjust API URL part to look for as McDonald's changed it in recent updates
- Add npm package `html-entities` as no longer used

## 2022-03-30 changes

- Modify `server:scrape` to:
  - Get hidden McDonald's Nutritional Calculator data by directly accessing JSON API using category IDs. This is necessary as previously shown categories have disappeared from view, but not from their API
  - Add single product scraping for `Mozzarella Dipper Sharebox` and `Galaxy Chocolate Mini McFlurry`
  - Remove single product scraping for `Quarter Pounder with Cheese` and `Double Quarter Pounder with Cheese` as other scraping now acquires this data
- Modify `server:build-client-data` to:
  - Try additional Just Eat item name to McDonald's nutritional item name resolutions, by swapping position of `Small`, `Large` and `Mini` to end of name if present
- Add further `src/server/jeItemNamesToMcItemNames.json` mappings as Just Eat added more items with differing names to their McDonald's nutritional counterpart
- Add npm package `html-entities` to decode HTML entities in string
- Update npm packages using `npm update` and `npm audit fix --force`

## 2022-02-22 changes

- Modify `server:scrape` to:
  - Fix issue that broke scraping some JustEat restaurant's pricing data. The structure of the JS object that the Just Eat restaurant pages sources it's menu data from changes, so before you could access `window.__INITIAL_STATE__` and that'd give you the root level data to then further descend, but now Just Eat are rolling out a change where you need to access `window.__INITIAL_STATE__.state` instead. `server:scrape` has been altered to detect when the new state structure has been served, and then correctly access the desired data on this
  - Ignore weirdly named "Crispy Chicken Salad - calories exc. additional condiments" item that is a meal (so should be ignored) but isn't named as such
- Map Just Eat name "Mozzarella Dippers Sharebox" to McDonald's nutritional "Mozzarella Dippers - Sharebox - 9 pieces"

## 2022-01-06 changes

- Dynamically convert Just Eat -> McDonald's McFlurry naming by always swapping order of "Mini McFlurry" to "McFLurry Mini". New McDonald's McFlurry products should automatically resolve their nutritional information now without manual fix. Remove redundant manual fixes also
- Fix README.md misspellings
- Fix McDonald's Nutritional Calculator scrape hang. Scraping code was waiting on dismissing a COVID-19 modal which they've now removed from the site, so removed this

## 2021-12-13 changes

- Support scraping nutritional data from Australian McDonald's Single Product pages by converting kcal to kJ and sodium to salt
- Scrape nutritional data for "Pineapple Stick" via Australian McDonald's Single Product page. Ideally a UK one should be used, but there isn't one
- Change project name from "McDonald's Menu Fun" to "McDonald's Menu Scraper"

## 2021-11-18 changes

- Update item naming mapping between Just Eat and McDonald's Nutritional Calculator to resolve a few more cases
- Modify `server:build-client-data` to:
  - Make resolving new item name mapping issues between Just Eat and McDonald's Nutritional Calculator easier. `server:scrape` normalises names, but `server:build-client-data` is the one that will expose name mapping issues, so altering `src/server/jeItemNamesToMcItemNames.json` to attempt to solve issues used to be a back-and-forth of running `server:scrape` then `server:build-client-data` then attempting to update mappings and repeat. Now `server:build-client-data` also normalises names, so no need to `server:scrape` many times, much quicker process
  - Not have to 100% match Just-Eat-item-name to Mcdonald's-nutritional-item-name. This allows script to run to completion when McDonald's removes nutritional item data availability (like when discontinuing an item's sale) but at selected restaurants continue to sell the item on Just Eat for a while, likely to clear their final inventory. This change will make it harder to spot correctable mapping errors, as previously I'd get a daily pipeline fail for each one, but that's fine, I'll aim to manually batch correct these issues on a regular schedule, maybe every end-of-month

## 2021-11-07 changes

- Initial app complete with envisioned feature set. Future change log entries will detail further changes like bug fixing and maintenance
